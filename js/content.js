var getKeys = (key) => {
	var blob = new Blob([key], {
		type: "text/plain;charset=UTF-8"
	});
	var url = window.URL.createObjectURL(blob);
	var a = document.createElement('a');
	a.href = url;
	a.target = '_blank';
	a.download = "OpenpgpBookmarks_CombinedKey.txt"
	a.id = "CombinedKey";
	document.body.appendChild(a);
	a.click();
}

var getBookmarks = (file) => {
	var blob = new Blob([file], {
		type: "text/plain;charset=UTF-8"
	});
	var url = window.URL.createObjectURL(blob);
	var a = document.createElement('a');
	a.href = url;
	a.target = '_blank';
	a.download = "Openpgpjs_Bookmarks.txt"
	a.id = "CombinedKey";
	document.body.appendChild(a);
	a.click();
}

var alertWindow = (jsoninfo) => {
	var cssText = `.swal-button {
		width: 150px !important;
		padding: 7px !important;
		background-color: darkorange !important;
		border-radius: 5px !important;
		border: 2px solid white !important;
		color: white !important;
		cursor: pointer !important;
		font-weight: bold !important;
	}
	.swal-button:hover {
		width: 150px !important;
		background-color: white !important;
		border: 2px solid darkorange !important;
		color: darkorange !important;
		cursor: pointer !important;
	}
	.swal-text {
		width: 90%;
		text-align: left;
		margin: auto;
		padding: 10px;
		background-color: #EDEDED;
		border: 1px solid lightgray;
		box-shadow: 1px 1px 15px gray inset;
		border-radius: 4px;
	}`
	var cssTextNode = document.createTextNode(cssText);
	var style = document.createElement('style');
	style.appendChild(cssTextNode);
	document.head.appendChild(style);
	var alertheader = jsoninfo.header;
	var alertmessage = jsoninfo.text;
	var alerttype = jsoninfo.type;
	swal(alertheader, alertmessage, alerttype);
}

var plainAlert = (text) => {
	alert(text);
}

var addScript = (script) => {
	var scrpt = document.createElement('script');
	scrpt.src = text;
	scrpt.id = '_script';
	document.head.appendChild(scrpt);
	alert(text);
}

var setLoginTime = (date) => {
	chrome.storage.local.set({
		'LoginTime': date
	});
}

var getLoginTime = () => {
	chrome.storage.local.get('LoginTime', (date) => {
		return date
	});
}

var setSessionInfo = (info) => {
	window.sessionStorage.setItem(info[0], info[1]);
	return "Complete";
}

var getSessionInfo = (info) => {
	var sessionInfo = window.sessionStorage.getItem(info);
	return {
		'tgp': sessionInfo
	};
}

chrome.runtime.onMessage.addListener(
	(request, sender, sendResponse) => {
		//console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");
		if(request.sendkeys) {
			getKeys(request.sendkeys);
			sendResponse({
				gotkeys: "received"
			});
		}
		if(request.sendBookmarks) {
			getBookmarks(request.sendBookmarks);
			sendResponse({
				gotBookmarks: "received"
			});
		}
		if(request.sendAlert) {
			alertWindow(request.sendAlert);
			sendResponse({
				gotAlert: "received"
			});
		}
		if(request.sendPlainAlert) {
			plainAlert(request.sendPlainAlert);
			sendResponse({
				gotPlainAlert: "received"
			});
		}
		if(request.sendSessionInfo) {
			sendResponse({
				gotSessionInfo: "received"//setSessionInfo(request.sendSessionInfo)
			});
		}
		if(request.getSessionInform) {
			sendResponse({
				gotSessionInform: getSessionInfo(request.getSessionInform)
			});
		}
	}
);

chrome.runtime.onConnect.addListener(function(port) {
	console.assert(port.name == "contentjs");
	port.onMessage.addListener(function(msg) {
		if(msg.test) {
			//runTest(msg.test);
			port.postMessage({
				ranTest: 'ran test'
			})
		}
		if(msg.sendSetLoginTime) {
			setLoginTime(msg.sendSetLoginTime);
			port.postMessage({
				gotSetLoginTime: msg.sendSetLoginTime
			});
		}
		if(msg.sendGetLoginTime) {
			port.postMessage({
				gotGetLoginTime: getLoginTime()
			});
		}
	});
});

