# Openpgpjs_Extension

### <img src="https://gitlab.com/rabel001/Applications/raw/master/Openpgpjs%20Encryptor/resources/app/images/favicon.png" width="20" height="20"> Openpgpjs_Encryption
* [![Electron badge](https://img.shields.io/badge/Made%20with-Electron-6a5acd.svg)](https://electronjs.org)
* [![Openpgpjs badge](https://img.shields.io/badge/Openpgpjs-v3.0.11-green.svg)](https://gitcdn.link/cdn/openpgpjs/openpgpjs/bf428b80d40ae3045ad40debe9798aeff98caa4e/dist/openpgp.js)  
* Encrypt plain text with pgp keys.