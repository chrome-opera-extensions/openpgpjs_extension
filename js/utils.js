((global, doc) => {
	'use strict';

	var _injectScript = (script) => {
		var help;
		if(typeof script == "object") {
			if(!!script.script) {
				help = script.help;
				script = script.script;
			}
		} else if(!/\/\//.test(script)) {
			console.log('injectScript Output:\n\t - Error: Script must be a url containing http/https.');
			return;
		}
		var promise = async (nscript) => {
			return new Promise((resolve, reject) => {
				var continuation = (nscript) => {
					try {
						var newScript = doc.createElement('script');
						newScript.type = 'text/javascript';
						newScript.className = 'newScript';
						newScript.src = nscript;
						newScript.onerror = function(event) {
							reject('injectScript output:\n\t-Error:\n\t\t- Script failed to load with error code "' + event.type + '".\n\t\t- Script Name: ' + nscript);
						};
						newScript.onload = function(event) {
							resolve(`injectScript output:\n\t- Loaded: true\n\t- Script Element: ${newScript.outerHTML}`);
						};
						doc.head.appendChild(newScript);
					} catch(err) {
						var err0 = 'injectScript Output:\n\t- Error loading ' + nscript.substring(nscript.lastIndexOf('/') + 1, nscript.length) + '\n\t- Error message: ' + err.message;
						reject(err0);
					}
				};
				if(!/\.js$/i.test(nscript)) {
					reject(`\t-Error:\n\t\t- File name: ${nscript}.\n\t\t- ERROR: File extension should by ".js".`);
				} else if(doc.getElementsByClassName('newScript').length !== 0) {
					var scripts = Array.from(doc.getElementsByClassName('newScript'));
					var arr = [];
					scripts.forEach((e) => {
						arr.push(e.src);
					});
					if(!!~arr.indexOf(nscript)) {
						reject('\t-Error: Script is already loaded');
					} else {
						continuation(nscript);
					}
				} else {
					continuation(nscript);
				}
			});
		};
		promise(script).then((data) => {
			console.log(data);
			if(help !== undefined) {
				console.log('\t- Script Help: ' + help);
			}
		}).catch((error) => {
			console.log(error);
		});
	};
	_injectScript.available_Scripts = {
		html5shiv: {
			script: 'https://rawgit.com/aFarkas/html5shiv/master/src/html5shiv.js',
			help: 'https://github.com/aFarkas/html5shiv',
		},
		jquery1113: {
			script: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.js',
			help: 'https://jquery.com'
		},
		jquery224: {
			script: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js',
			help: 'https://jquery.com'
		},
		jquery300: {
			script: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.js',
			help: 'https://jquery.com'
		},
		underscore: {
			script: 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js',
			help: 'http://underscorejs.org'
		},
		angular: {
			script: 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js',
			help: 'https://github.com/angular/angular.js'
		},
		platform: {
			script: 'https://rawgit.com/bestiejs/platform.js/master/platform.js',
			help: 'https://github.com/bestiejs/platform.js'
		},
		bowser: {
			script: 'https://rawgit.com/ded/bowser/master/src/bowser.js',
			help: 'https://github.com/ded/bowser'
		},
		arrive: {
			script: 'https://rawgit.com/uzairfarooq/arrive/master/src/arrive.js',
			help: 'https://github.com/uzairfarooq/arrive'
		},
		zest: {
			script: 'https://rawgit.com/chjj/zest/master/lib/zest.js',
			help: 'https://github.com/chjj/zest'
		},
		HTML: {
			script: 'https://rawgit.com/nbubna/HTML/master/dist/HTML.js',
			help: 'http://nbubna.github.io/HTML'
		},
		head: {
			script: 'https://cdnjs.cloudflare.com/ajax/libs/headjs/1.0.3/head.min.js',
			help: 'http://headjs.com'
		},
		tags: {
			script: 'https://rawgit.com/a-tarasyuk/tag/master/build/tag.js',
			help: 'https://github.com/a-tarasyuk/tag'
		},
		sugar: {
			script: 'https://rawgit.com/andrewplummer/Sugar/master/dist/sugar.js',
			help: 'https://sugarjs.com'
		},
		requirejs: {
			script: 'https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.5/require.min.js',
			help: 'https://requirejs.org'
		},
		detectBrowser: {
			script: 'https://rawgit.com/darcyclarke/Detect.js/master/detect.js',
			help: 'https://github.com/darcyclarke/Detect.js'
		},
		is: {
			script: 'https://rawgit.com/arasatasaygin/is.js/master/is.js',
			help: 'http://is.js.org'
		},
		xregexp: {
			script: 'https://rawgit.com/slevithan/xregexp/master/xregexp-all.js',
			help: 'https://github.com/slevithan/xregexp/blob/master/README.md'
		},
		respond: {
			script: 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
			help: 'https://github.com/scottjehl/Respond'
		},
		dataTables: {
			script: 'https//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js',
			help: 'https://www.datatables.net'
		}
	};
	for(var i in _injectScript.available_Scripts) {
		_injectScript.available_Scripts[i].inject = function() {
			_injectScript(this);
		};
	}
	global.injectScript = _injectScript;
})(window, document);