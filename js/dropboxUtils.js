//import openpgp from "../../../../node_modules/@types/openpgp";
//import Dropbox from "../../../../node_modules/@types/dropboxjs";
ucurrentfile = "Running from dropboxUtils.js";

((global) => {
	global.dropboxUtils = {
		authenticate: async (client) => {
			console.log(ucurrentfile + ': authenticate');
			var writeToPage = this.dropboxUtils.writeToPage
			dropboxManager.getToken(client).then((token) => {
				console.log('authenticate: uploading bookmarks');
				chrome.storage.local.set({
					'syncStorage': 'dropbox'
				});
				dropboxUtils.uploadBookmarks(client);
			}).catch(async (error) => {
				/*var result = await dropboxManager.interactiveLogin(client);
				var resp;
				try {
					if(/AccessToken/.test(JSON.stringify(result))) {
						resp = "Authenticated!";
					}
				} catch {
					console.error(ucurrentfile + ': authenticate');
					resp = result;
				}
				writeToPage(resp, true);
				console.log('authenticate catch: uploading bookmarks');
				dropboxUtils.uploadBookmarks(client);*/
			});
		},
		creatBlob: (file) => {
			var blob = new Blob([file], {
				type: "text/plain;charset=UTF-8"
			});
			var blob = window.URL.createObjectURL(blob);
			return blob;
		},
		uploadBookmarks: async (
			client) => {
			var writeToPage = this.dropboxUtils.writeToPage
			writeToPage('Retrieving bookmarks.', true);
			var EBMFOLDER;
			var lines;
			chrome.storage.local.get('EBMFOLDER', (result) => {
				console.log("Running uploadBookmarks");
				if(result.EBMFOLDER === undefined) {
					console.log("Bookmarks are not set.");
					return;
				} else {
					EBMFOLDER = JSON.parse(atob(result.EBMFOLDER));
					var i = 1;
					console.log("EBMFOLDER ID: " + EBMFOLDER.id);
					chrome.bookmarks.getChildren(EBMFOLDER.id, async (results) => {
						results.forEach((item) => {
							lines = (lines == null) ? `[Bookmark${i}]` + '\r\n' + item.title + '\r\n' : lines + `[Bookmark${i}]` + '\r\n' + item.title + '\r\n';
							i++
						});
						var bytes = dropboxManager.stringToBytes(lines);
						console.log(bytes);
						await dropboxManager.setdbx(client);
						dropboxUtils.upload(client, lines, "Openpgpjs_Bookmarks.js")
					});
				}
			});
		},
		uploadFile: async (client) => {
			console.log(ucurrentfile + ': uploadFile');
			var fileInput = document.getElementById('file-upload');
			if(fileInput.files.length !== 0) {
				var file = fileInput.files[0];
				global.file = file;
				writeToPage(file);
				//await dropboxManager.setdbx(client);
				//upload(client, file, file.name);
			}
		},
		testWriteToPage: (multiple) => {
			var writeToPage = this.dropboxUtils.writeToPage
			if(multiple) {
				writeToPage('First line', true);
				writeToPage('Second line', true);
				writeToPage('Third line', true);
			} else {
				writeToPage('Third line', true);
				writeToPage('Overwritten', false);
			}
		},
		upload: (client, contents, filename) => {
			console.log(ucurrentfile + ': upload');
			var writeToPage = this.dropboxUtils.writeToPage;
			writeToPage('upload: Uploading bookmarks.', true);
			dropboxManager.setdbx(client).then((response) => {
				var options = {
					path: '/' + filename,
					contents: contents,
					mode: 'overwrite'
				}
				console.log(options);
				dropboxManager.dbx.filesUpload(options).then((response) => {
						writeToPage('Bookmarks uploaded!', true);
						console.log(response);
					})
					.catch((error) => {
						writeToPage('Error in upload method.', true);
						if(typeof error === "string") {
							writeToPage(error, false);
						} else {
							writeToPage(JSON.stringify(error), false);
						}
						console.error(ucurrentfile + ': uploadFile');
						console.error('Error in upload');
						console.error(error);
					});
			}).catch((error) => {
				return error;
			});
		},
		writeToPage: (text, multiple) => {
			var resultselm = document.getElementById('results');
			if(multiple) {
				resultselm.appendChild(document.createTextNode(text));
				resultselm.appendChild(document.createElement('br'));
			} else {
				resultselm.innerText = text;
			}
		},
		sync: () => {
			dropboxUtils.getSync().then((result) => {
				if(result) {

				}
			});
		}
	}

	//curl -X POST https://api.dropboxapi.com/2/file_requests/create \
	//--header "Authorization: Bearer Xa-ZqQfDZ2kAAAAAAAAPb6c2Fu0GcvmZs5CYW9XVq9hRAMcHreZY9RDTZbYj3sFY" \
	//--header "Content-Type: application/json" \

	//addEventListener('DOMContentLoaded', () => {
	global.addEventListener("load", () => {
		chrome.storage.local.get('syncStorage', (result) => {
			console.log(result);
			if(result.syncStorage === 'dropbox') {
				dropboxManager.getSync().then((result) => {
					if(result) {
						document.getElementById('sync-checkbox').checked = true;
						document.getElementById('sync-apply').value = "Disable Sync";
					}
				});
			}
		});
		document.getElementById('file').addEventListener('click', function() {
			document.getElementById('file-upload').click();
		});
		document.getElementById('fs-dropbox').addEventListener('click', () => {
			dropboxManager.clients.set(dropboxManager.clients.type.dropbox);
			var client = dropboxManager.clients.get();
			dropboxUtils.authenticate(client);
		});
		document.getElementById('fs-filesystem').addEventListener('click', () => {
			document.getElementById('file-upload').click();
			//dropboxManager.clients.set(dropboxManager.clients.type.dropbox);
			//var client = dropboxManager.clients.get();
			//dropboxUtils.authenticate(client);
		});
		document.getElementById('btn').addEventListener('click', () => {
			dropboxManager.clients.set(dropboxManager.clients.type.dropbox);
			var client = dropboxManager.clients.get();
			dropboxUtils.uploadFile(client);
		});
		document.getElementById('auth').addEventListener('click', () => {
			dropboxManager.clients.set(dropboxManager.clients.type.dropbox);
			var client = dropboxManager.clients.get();
			dropboxUtils.authenticate(client);
		});
		document.getElementById('sync-apply').addEventListener('click', (evt) => {
			if(document.getElementById('sync-apply').value === "Apply Sync") {
				if(document.getElementById('sync-checkbox').checked) {
					dropboxManager.setSync(true).then((enabled) => {
						dropboxUtils.writeToPage("Sync enabled.");
						document.getElementById('sync-apply').value = "Disable Sync";
					});
				}
			} else if(document.getElementById('sync-apply').value === "Disable Sync") {
				document.getElementById('sync-checkbox').checked = false;
				dropboxManager.setSync(false).then((disabled) => {
					document.getElementById('sync-apply').value = "Apply Sync"
					dropboxUtils.writeToPage("Sync disabled.");
				});
			}
		})
	});
})(window);


/*
var a = dropboxUtils.parseQueryString(authUrl);
var dbx = new Dropbox.Dropbox({
	accessToken: r.dropboxAccessToken,
	clientId: a.client_id
});

//list files
dbx.filesListFolder({
		path: ''
	})
	.then(function(response) {
		console.log(response.entries);
	})
	.catch(function(error) {
		console.error(error);
	});

//Get current user account
dbx.usersGetCurrentAccount()
	.then(function(response) {
		console.log(response);
	})
	.catch(function(error) {
		console.error(error);
    });

dbx.filesUpload({path: '/' + file.name, contents: file})
    .then(function(response) {
      var results = document.getElementById('results');
      results.appendChild(document.createTextNode('File uploaded!'));
      console.log(response);
    })
    .catch(function(error) {
      console.error(error);
    });
*/