((_g, _d, _c) => {
	'use strict';
	var opgpbms = function() {
		return {
			isOtherBMsSet: false,
			isEncryptedBMsSet: false,
			geid: _d.getElementById.bind(_d),
			gecn: _d.getElementsByClassName.bind(_d),
			getn: _d.getElementsByTagName.bind(_d),
			gqsa: _d.querySelectorAll.bind(_d),
			gqs: _d.querySelector.bind(_d),
			sleep: (time) => {
				return new Promise((resolve) => setTimeout(resolve, time));
			},
			logDebug: (info, error = false) => {
				if(!console.logDebug) {
					console.logDebug = _g.opgpbms.logDebug;
				}
				var enableDebug = true;
				if(enableDebug) {
					if(error) {
						console.error(info);
					} else {
						console.log(info);
					}
				}
			},
			setLoginTime: () => {
				var date = new Date();
				date = date.toString();
				chrome.storage.local.set({
					'LoginTime': date
				});
			},
			cpd: () => {
				///////////////////////////////////
				/////      changePassword     /////
				///////////////////////////////////
				///// Change password and     /////
				///// re-encrypt bookmarks    /////
				///////////////////////////////////
				_g.opgpbms.logDebug('Running cpd');
				var p1 = _g.opgpbms.geid('t-changepwd1').value;
				var p2 = _g.opgpbms.geid('t-changepwd2').value;
				//if(p1 === null || p1 === "" || p2 === null || p2 === "") {
				if(!p1 || !p2) {
					swal('Error', 'Passwords cannot be empty.', 'error');
					_g.opgpbms.gecn('swal-text')[0].style.textAlign = "center";
				} else if(p1 !== p2) {
					swal('Error', 'Passwords do not match.', 'error');
					_g.opgpbms.gecn('swal-text')[0].style.textAlign = "center";
				} else {
					swal('Please wait', 'Building encryption keys.', 'warning', {
						buttons: false
					})
					_c.storage.local.get('publicKey', (pub) => {
						var pubkey = pub.publicKey;
						pubkey = atob(pubkey);
						var userid = openpgp.key.readArmored(pubkey).keys[0].users[0].userId.userid;
						userid = userid.match(/<.*>/)[0].replace('<', '').replace('>', '');
						var options = {
							userIds: [{
								name: 'Openpgpjs Bookmark user',
								email: userid
							}],
							numBits: 4096,
							passphrase: p1
						};
						var keyhash = {};
						openpgp.generateKey(options).then((key) => {
							keyhash.privkey = key.privateKeyArmored.trim();
							keyhash.pubkey = key.publicKeyArmored.trim();
							var swalhash = {
								title: 'Success',
								text: 'Bookmarks decrypted.'
							}
							_g.opgpbms.dBkmks(swalhash).then((bookmarks) => {
								bookmarks.forEach((bookmark) => {
									if(bookmark.url !== "Folder") {
										var title = bookmark.title;
										var url = bookmark.url;
										var id = bookmark.id;
										var optionsTitle = {
											data: title,
											publicKeys: openpgp.key.readArmored(keyhash.pubkey).keys
										};
										openpgp.encrypt(optionsTitle).then((ciphertextTitle) => {
											var optionsUrl = {
												data: url,
												publicKeys: openpgp.key.readArmored(keyhash.pubkey).keys
											};
											openpgp.encrypt(optionsUrl).then((ciphertextUrl) => {
												var base64title = btoa(ciphertextTitle.data);
												var base64url = btoa(ciphertextUrl.data);
												_c.bookmarks.update(id, {
													'title': `${base64title}:${base64url}`
												});
											});
										});
									}
								});
								_c.storage.local.set({
									privateKey: btoa(keyhash.privkey)
								});
								_c.storage.local.set({
									publicKey: btoa(keyhash.pubkey)
								});
								_g.opgpbms.settgp(p1);
								swal('Congratulations!', 'Your password has been changed.\nMake sure to export your bookmarks and keys.', 'success').then(() => {
									_g.opgpbms.geid('t-changepwd1').value = "";
									_g.opgpbms.geid('t-changepwd2').value = "";
									_g.opgpbms.geid('p-changepwd').classList.add('o-hidden');
									_g.opgpbms.geid('p-changepwd').style.display = 'none';
									_g.opgpbms.geid('i-checkbox').checked = false;
									_g.opgpbms.geid('ipass-checkbox').checked = false;
									_g.opgpbms.getn('details')[0].removeAttribute('open');
								});
							}).catch(function(error) {
								_g.opgpbms.logDebug(error);
							});
						});
					});
				}
			},
			gobkmks: () => {
				_g.opgpbms.logDebug('Running gobkmks');
				return new Promise((resolve) => {
					var foldername = "Other bookmarks";
					_c.bookmarks.getTree((bmTree) => {
						bmTree[0].children.forEach((child) => {
							if(/OPR/.test(_g.navigator.userAgent)) {
								foldername = "Imported bookmarks";
							}
							if(child.title === foldername) {
								_c.storage.local.set({
									OTHERBOOKMARKS: child
								});
								_g.opgpbms.isOtherBMsSet = true;
								resolve(true);
							}
						})
					});
				});
			},
			getEBMID: () => {
				///////////////////////////////
				/////   chrome.getEBMID   /////
				///////////////////////////////
				///// Get the ID  of the  ///// 
				///// Encrypted Bookmarks ///// 
				///// folder and setItem  ///// 
				///////////////////////////////
				_g.opgpbms.logDebug('Running getEBMID');
				var continuation = () => {
					return new Promise(function(accept) {
						_c.storage.local.get('OTHERBOOKMARKS', (result) => {
							_c.bookmarks.getChildren(result.OTHERBOOKMARKS.id, (results) => {
								var EBMFOLDER = {};
								for(var i = 0; i < results.length; i++) {
									if(results[i].title === "Encrypted Bookmarks") {
										EBMFOLDER = btoa(JSON.stringify(results[i]));
										_c.storage.local.set({
											EBMFOLDER: EBMFOLDER
										});
										_g.opgpbms.isEncryptedBMsSet = true;
										accept(true);
									}
								}
							});
						});
					});
				}
				return new Promise(function(resolve) {
					if(!_g.opgpbms.isOtherBMsSet) {
						_g.opgpbms.gobkmks().then(() => {
							continuation().then(() => {
								resolve(true);
							}).catch(function(error) {
								_g.opgpbms.logDebug(error);
							});
						}).catch(function(error) {
							_g.opgpbms.logDebug(error);
						});
					} else {
						continuation().then(() => {
							resolve(true);
						}).catch(function(error) {
							_g.opgpbms.logDebug(error);
						});
					}
				});
				/*if(!_g.opgpbms.isOtherBMsSet) {
					_g.opgpbms.gobkmks();
					var cycle = _g.setInterval(() => {
						if(_g.opgpbms.isOtherBMsSet) {
							continuation();
							_g.clearInterval(cycle);
						}
					}, 100);
				} else {
					continuation();
				} */
			},
			buildPage: () => {
				///////////////////////////////////
				/////        buildPage       /////
				///////////////////////////////////
				///// Get encrypted bookmarks /////
				///// bookmarks and pass them /////
				///// to processNode to build /////
				///// the bookmarks table     /////
				///////////////////////////////////
				_g.opgpbms.logDebug('Running buildPage');
				return new Promise(resolve => {
					var swalhash = {
						title: 'Congratulations',
						text: 'Bookmark table is loaded'
					}
					swal('Please wait', 'Decrypting bookmarks...', 'warning', {
						buttons: false
					});
					//return new Promise(resolve => {
					console.time('buildPage');
					if(!_g.sessionStorage.getItem('db')) {
						_g.opgpbms.dBkmks(swalhash).then((oHashArray) => {
							var list = _g.opgpbms.gqs("#bookmarklist");
							var th = list.querySelector('thead > tr > th:first-child');
							var tbody = list.querySelector('tbody');
							var db = [];
							_g.sessionStorage.setItem('db', JSON.stringify(db));
							oHashArray.forEach((e, i) => {
								if(e.url !== 'Folder') {
									var newnode = `<tr id="_${e.id}"><td>` +
										`<a href="${e.url}" target="_blank">${e.title}</a>` +
										`</td><td>` +
										`<a id="dlt" class="_${e.id}" href="#">Delete</a>` +
										`</td></tr>`
									tbody.innerHTML += newnode;
									var db = _g.sessionStorage.getItem('db');
									db = JSON.parse(db);
									db = JSON.stringify(db.concat({
										'id': e.id,
										'title': e.title,
										'url': e.url
									}));
									_g.sessionStorage.setItem('db', db);
								}
							});
							_g.dtable = $('#bookmarklist').DataTable({
								"stripeClasses": ['odd-row', 'even-row']
							});
							_g.opgpbms.gqs('#d-bookmarklist').classList.remove('o-hidden');
							_g.opgpbms.gqs('#importbms').classList.remove('o-hidden');
							_g.opgpbms.saveHtML().then((page) => {
								_g.sessionStorage.setItem('bkmkpage', btoa(page));
							});
							console.timeEnd('buildPage');
							resolve(true);
						}).catch(function(error) {
							_g.opgpbms.logDebug(error);
							resolve(false);
						});
					} else {
						var list = _g.opgpbms.gqs("#bookmarklist");
						var th = list.querySelector('thead > tr > th:first-child');
						var tbody = list.querySelector('tbody');
						var db = _g.sessionStorage.getItem('db');
						db = JSON.parse(db);
						db.forEach((e) => {
							var newnode = `<tr id="_${e.id}"><td>` +
								`<a href="${e.url}" target="_blank">${e.title}</a>` +
								`</td><td>` +
								`<a id="dlt" class="_${e.id}" href="#">Delete</a>` +
								`</td></tr>`
							tbody.innerHTML += newnode;
						});
						_g.dtable = $('#bookmarklist').DataTable({
							"stripeClasses": ['odd-row', 'even-row']
						});
						_g.opgpbms.gqs('#d-bookmarklist').classList.remove('o-hidden');
						_g.opgpbms.gqs('#importbms').classList.remove('o-hidden');
						//_g.opgpbms.saveHtML().then((page) => {
						// 	_g.sessionStorage.setItem('bkmkpage', btoa(page));
						//});
						console.timeEnd('buildPage');
						swal({
							title: swalhash.title,
							text: swalhash.text,
							icon: 'success',
							timer: 2000
						});
						resolve(true);
					}
				});
				//});
			},
			dBkmk: (item) => { /*async*/
				/////////////////////////////
				/////  decryptBookMark  /////
				/////////////////////////////
				///// Decrypt bookmarks /////
				/////////////////////////////
				_g.opgpbms.logDebug('Running dBkmk');
				//return new Promise(resolve => {
				return new Promise((resolve) => {
					_c.storage.local.get('privateKey', (priv) => {
						_c.storage.local.get('publicKey', (pub) => {
							var pubkey = pub.publicKey;
							pubkey = atob(pubkey);
							var privkey = priv.privateKey;
							privkey = atob(privkey);
							var privKeyObj = openpgp.key.readArmored(privkey).keys[0];
							//var tgp = _g.opgpbms.gtgp();
							var r = _g.sessionStorage.getItem('tgp');
							var c = JSON.parse(r);
							var o = atob((c.endplan.substring(0, c.length)).split('').reverse().join(''));
							var tgp = o.split('').reverse().join('');
							privKeyObj.decrypt(tgp).then((pbool) => {
								if(item !== undefined) {
									item = atob(item);
									//console.log(item);
									var options = {
										message: openpgp.message.readArmored(item),
										publicKeys: openpgp.key.readArmored(pubkey).keys,
										privateKeys: [privKeyObj]
									};
									openpgp.decrypt(options).then((plaintext) => {
										resolve(plaintext.data);
									}, (error) => {
										resolve(error);
									});
								}
							});
						});
					});
				});
				//});
			},
			dBkmks: (swalhash) => {
				///////////////////////////////////
				/////       decryptBkmks      /////
				///////////////////////////////////
				///// Decrypt bookmarks and   /////
				///// and return an array     /////
				///////////////////////////////////
				_g.opgpbms.logDebug('Running dBkmks');
				//return new Promise(resolve => {
				return new Promise((resolve) => {
					window.bookmarks = "bookmarks";
					_c.bookmarks.getTree((bmTree) => {
						var tabledone = false;
						var foldername = "Other bookmarks";
						var oHashArray = [];
						var treeChildren = bmTree[0].children;
						for(let i = 0; i < treeChildren.length; i++) {
							var child = treeChildren[i];
						//bmTree[0].children.map(child => {
							if(/OPR/.test(_g.navigator.userAgent)) {
								foldername = "Imported bookmarks";
							}
							if(child.title === foldername) {
								_c.bookmarks.getChildren(child.id, (results) => {
									var EBMFOLDER = {};
									results.map(item => {
										if(item.title === "Encrypted Bookmarks") {
											EBMFOLDER = btoa(JSON.stringify(item));
											_c.storage.local.get('EBMFOLDER', (e) => {
												if(e.EBMFOLDER == undefined) {
													_c.storage.local.set({
														EBMFOLDER: EBMFOLDER
													});
												}
											});
											var oHash = {
												id: item.id,
												url: 'Folder',
												title: item.title + " (Folder)"
											}
											oHashArray.push(oHash);
											_c.bookmarks.getChildren(item.id, (bookmarks) => {
												if(!bookmarks.length) {
													setTimeout(() => {
														swal({
															title: swalhash.title,
															text: swalhash.text,
															icon: 'success',
															timer: 2000
														}).then(() => {
															resolve(oHashArray);
														});
													}, 5000);
												}
												//bookmarks.forEach(async (bookmark) => {
												//window.bookmarks = bookmarks;
												//var p = new Parallel(bookmarks);
												//p.map(function(bookmark) {
												//	console.log(bookmark);
												bookmarks.forEach((bookmark) => {
													//var dec = await _g.opgpbms.dBkmk(bookmark.title);
													_g.opgpbms.dBkmk(bookmark.title).then((dec) => {
														var dt = dec.split(/\$\|\$/)[0];
														var du = dec.split(/\$\|\$/)[1];
														var oHash = {
															id: bookmark.id,
															url: du,
															title: dt
														};
														oHashArray.push(oHash);
														if(oHashArray.length >= bookmarks.length) {
															setTimeout(() => {
																swal({
																	title: swalhash.title,
																	text: swalhash.text,
																	icon: 'success',
																	timer: 2000
																}).then(() => {
																	resolve(oHashArray);
																});
															}, 5000);
														}
													}).catch(function(error) {
														_g.opgpbms.logDebug(error);
													});
												});
											});
										}
									});
								});
							};
						}//});
					});
				});
			},
			saveHtML: () => {
				return new Promise((resolve) => {
					var pgt = _g.sessionStorage.getItem('tgp');
					var rgt = JSON.parse(pgt);
					var cgt = atob((rgt.endplan.substring(0, rgt.length)).split('').reverse().join(''));
					var tgp = cgt.split('').reverse().join('');
					_c.storage.local.get(('privateKey'), (priv) => {
						_c.storage.local.get('publicKey', (pub) => {
							var pubkey = atob(pub.publicKey);
							var options = {
								data: document.body.parentElement.outerHTML,
								publicKeys: openpgp.key.readArmored(pubkey).keys
							};
							openpgp.encrypt(options).then((ciphertext) => {
								resolve(ciphertext.data);
							});
						});
					});
				});
			},
			/*gettgp: () => {
				//_g.console.log('Running getTheGamePlan');
				/////////////////////////////
				/////  getTheGamePlan   /////
				/////////////////////////////
				///// Get The Game Plan /////
				/////////////////////////////
				var rev = _g.sessionStorage.getItem('tgp');
				rev = JSON.parse(rev);
				rev = atob((rev.endplan.substring(0, rev.length)).split('').reverse().join(''));
				var tgp = rev.split('').reverse().join('');
				return tgp;
			},*/
			settgp: (tgp) => {
				/////////////////////////////
				/////  setTheGamePlan   /////
				/////////////////////////////
				///// Set The Game Plan /////
				/////////////////////////////
				_g.opgpbms.logDebug('Running settgp');
				if(tgp == '') return;
				var y = `Mm4cQ@c*ubXnu2pECads2CJ%Z!UH0Ba8KE9P2EgU@mmS4Hxmhv!WBVtnkBDp0NkWnzyEJm*A%A$Etkk$fUHY7Th?XDN57J*5*f*`;
				y = btoa(y);
				var preplan = btoa(tgp.split('').reverse().join('')).split('').reverse().join('');
				var midplan = preplan + y;
				var rev = {
					"length": preplan.length,
					"endplan": midplan
				}
				rev = JSON.stringify(rev);
				_g.sessionStorage.setItem('tgp', rev);
				chrome.tabs.query({
					active: true,
					currentWindow: true
				}, (tabs) => {
					chrome.tabs.sendMessage(tabs[0].id, {
						setSessionInfo: ['tgp', rev]
					});
				});
			},
			testPwd: () => {
				/////////////////////////
				/////    testPwd    /////
				/////////////////////////
				///// TEST Password /////
				/////////////////////////
				_g.opgpbms.logDebug('Running testPwd');
				return new Promise((resolve) => {
					_c.storage.local.get('LoginTime', (lt) => {
						var loginTime = new Date(lt.LoginTime);
						var now = new Date();
						if((now.getHours() - loginTime.getHours()) < 1) {
							_g.opgpbms.gqs('#submit').classList.add('o-hidden');
							resolve(true);
						}
					});
					if(_g.sessionStorage.getItem('openpgpjs_loggedin') != null && _g.sessionStorage.getItem('new_openpgpjsbm') == null) {
						_g.opgpbms.gqs('#submit').classList.add('o-hidden');
						resolve(true);
						//return;
					} else {
						//return new Promise(resolve => {
						var pwd = _g.opgpbms.geid("login-password").value;
						_g.opgpbms.settgp(pwd);
						_c.storage.local.get('privateKey', (verify) => {
							var privkey = atob(verify.privateKey)
							var oPrivKey = openpgp.key.readArmored(privkey).keys[0];
							oPrivKey.decrypt(pwd).then((verifyagain) => {
								resolve(true);
								_g.opgpbms.setLoginTime();
							}).catch((err) => {
								resolve(false);
							});
						});
						//});
					}
				});
			},
			//loginToAcct: async () => {
			loginToAcct: () => {
				//////////////////////////////
				/////     loginToAcct    /////
				//////////////////////////////
				///// Check if session   /////
				///// is already started /////
				///// and login          /////
				///// accordingly        /////
				//////////////////////////////
				_g.opgpbms.logDebug('Running loginToAcct');
				if(_g.opgpbms.gqs('#login-password').value === "") {
					swal('Error', 'Passwords cannot be empty.', 'error');
					return;
				}
				_g.opgpbms.gqs('#message').innerText = "";
				if(_g.sessionStorage.getItem('tgp') != null && _g.sessionStorage.getItem('tgp') !== "") {
					_g.opgpbms.testPwd().then((isCorrectPass) => {
						if(isCorrectPass) {
							if(_g.sessionStorage.getItem('openpgplist') == null) {
								_g.opgpbms.logDebug('loginToAcct if');
								_g.opgpbms.buildPage().then((bool) => {
									if(bool) {
										//_g.opgpbms.setLoginTime();
									}
								})
							}
							_g.opgpbms.gqs('#submit').classList.add('o-hidden');
							_g.opgpbms.gqs('#d-bookmarklist').classList.remove('o-hidden');
							_g.sessionStorage.setItem('openpgpjs_loggedin', true);
							_g.opgpbms.gqs('#importbms').classList.remove('o-hidden');

						} else {
							swal('Password Error', 'Password is incorrect', 'error');
							//_d.querySelector('#message').innerText = "Password is incorrect";
						}
					}).catch(function(error) {
						_g.opgpbms.logDebug(error, true);
					});
				} else {
					_c.storage.local.get('privateKey', (verify) => {
						if(verify.privateKey !== undefined) {
							_g.opgpbms.testPwd().then((isCorrectPass) => {
								if(isCorrectPass) {
									if(_g.sessionStorage.getItem('openpgplist') == null) {
										_g.opgpbms.logDebug('loginToAcct else');
										_g.opgpbms.buildPage().then((bool) => {
											if(bool) {
												//_g.opgpbms.setLoginTime();
											}
										})
									}
									_g.opgpbms.gqs('#submit').classList.add('o-hidden');
									_g.opgpbms.gqs('#d-bookmarklist').classList.remove('o-hidden');
									_g.sessionStorage.setItem('openpgpjs_loggedin', true);
									_g.opgpbms.gqs('#importbms').classList.remove('o-hidden');
								} else {
									swal('Password Error', 'Password is incorrect', 'error');
									//_d.querySelector('#message').innerText = "Password is incorrect";
								}
							}).catch(function(error) {
								_g.opgpbms.logDebug(error, true);
							});
						}
					});
				}
			},
			checkForNewUser: () => {
				///////////////////////////
				///// checkForNewUser /////
				///////////////////////////
				///// Check if first  /////
				///// time login and  /////
				///// enable proper   /////
				///// elements        /////
				///////////////////////////
				_g.opgpbms.logDebug('Running checkForNewUser');
				_c.storage.local.get('publicKey', (passwordCheck) => {
					if(passwordCheck.publicKey !== undefined) {
						_g.opgpbms.gqs('#create-password').classList.add('o-hidden');
						_g.opgpbms.gqs('#submit').classList.remove('o-hidden');
						_g.opgpbms.geid("login-account").addEventListener("click", _g.opgpbms.loginToAcct);
						if(_g.sessionStorage.getItem('openpgpjs_loggedin') != null) {
							_g.opgpbms.gqs('#submit').classList.add('o-hidden');
							_g.opgpbms.logDebug('checkForNewUser: buildPage');
							_g.opgpbms.buildPage().then((bool) => {
								if(bool) {
									_g.opgpbms.setLoginTime();
								}
							})
						}
					} else {
						_g.opgpbms.gqs('#submit').classList.add('o-hidden');
						_g.opgpbms.gqs('#create-password').classList.remove('o-hidden');
						_g.opgpbms.geid("setup-account").addEventListener("click", _g.opgpbms.generateKeyPair);
					}
				});
			},
			handleImportBkmks: (evt) => {
				////////////////////////////////
				/////   handleImportBkmks  /////
				////////////////////////////////
				///// Handles uploaded     /////
				///// Encryption bookmarks /////
				////////////////////////////////
				_g.opgpbms.logDebug('Running handleImportBkmks');
				var file = evt.target.files[0];
				if(file === undefined) return;
				if(!/text\/plain/.test(file.type) && file.type !== '') {
					swal('File Error', `Wrong file type: '${file.type}'\nFile type must be: 'text/plain'`, 'error')
					return;
				}
				var reader = new FileReader();
				reader.onload = (event) => {
					var iFile = event.target.result;
					_g.iFile = iFile;
					if(!(/\[Bookmark\d+\]/).test(iFile)) {
						swal('File Error', 'Files are not in the correct format.', 'error');
						reader.abort();
						return;
					} else {
						var nFile = [];
						iFile.split(/\[Bookmark\d+\]/).forEach((e) => {
							if(e !== "") {
								nFile.push(e.trim());
							}
						});
						_c.storage.local.get('EBMFOLDER', (result) => {
							var EBMFOLDER = JSON.parse(atob(result.EBMFOLDER));
							var EBMID = EBMFOLDER.id;
							_c.bookmarks.getChildren(EBMID, (children) => {
								_g.opgpbms.children = children;
								var contains = (iNeedle, iArray) => {
									for(var i in iArray) {
										if(iNeedle === iArray[i].title) {
											return true;
										}
									}
									return false;
								}
								for(var i = 0; i <= nFile.length; i++) {
									(async (i) => {
										//setTimeout(async () => {
										setTimeout(() => {
											try {
												if(!contains(nFile[i], children)) {
													//var decryptedBookmark = await _g.opgpbms.dBkmk(nFile[i]);
													_g.opgpbms.dBkmk(nFile[i]).then((decryptedBookmark) => {
														decryptedBookmark = decryptedBookmark.split(/\$\|\$/)[0];
														if(/error/i.test(decryptedBookmark)) {
															_g.opgpbms.geid('p-error').innerText += `[Bookmark${i+1}] error:\n ${decryptedBookmark}` + '\n';
															//alert(`[Bookmark${n}] error:\n ${decryptedBookmark}`);
														} else {
															_c.bookmarks.create({
																'title': nFile[i],
																'url': 'http://pgp.encyrpyted.bookmark',
																'parentId': EBMID
															});
														}
														if(i == (nFile.length - 1)) {
															setTimeout(() => {
																_g.sessionStorage.setItem('BmarkImportErrors', _g.opgpbms.geid('p-error').innerText);
															}, i * 1200);
														}
													}).catch(function(error) {
														_g.opgpbms.logDebug(error);
													});
												} else {
													_g.opgpbms.geid('p-error').innerText += `[Bookmark${i+1}]` + '\n';
													swal('Not Added', `[Bookmark${i+1}] is already bookmarked.`, 'warning');
												}
											} catch (err) {}
											if(i == (nFile.length - 1)) {
												setTimeout(() => {
													if(_g.opgpbms.geid('p-error').innerText !== "") {
														_g.sessionStorage.setItem('BmarkImportErrors', _g.opgpbms.geid('p-error').innerText);
														swal('Not Added', _g.opgpbms.geid('p-error').innerText);
														_g.opgpbms.gqs('.swal-text').classList.add('swal-overflow');
													}
													_g.opgpbms.geid('i-checkbox').checked = false;
													_g.opgpbms.geid('p-importbms').classList.add('o-hidden');
													_g.opgpbms.getn('details')[0].removeAttribute('open');
													_g.opgpbms.geid('p-error').innerText = "";
													_g.sessionStorage.removeItem('BmarkImportErrors');
												}, 1400);
											}
										}, i * 1200);
									})(i);
								}
							});
						});
					}
				}
				reader.onerror = (evt) => {
					swal('File error', evt.target.error.name, 'error');
					reader.abort();
				};
				reader.readAsText(file);
			},
			handleImportKeys: (evt) => {
				//////////////////////////////
				/////  handleImportKeys  /////
				//////////////////////////////
				///// Handle uploaded    /////
				///// Encryption Keys    /////
				//////////////////////////////
				_g.opgpbms.logDebug('Running handleImportKeys');
				//try {
				var file = evt.target.files[0];
				if(file === undefined) return;
				if(!/text\/plain/.test(file.type) && file.type !== '') {
					swal('File Error', `Wrong file type: '${file.type}'\nFile type must be: 'text/plain'`, 'error')
					return;
				}
				var reader = new FileReader();
				reader.onload = (event) => {
					var iFile = event.target.result;
					var publines, privlines;
					var publicKey = /-----BEGIN\sPGP\sPUBLIC\sKEY\sBLOCK-----[\s\S]*-----END\sPGP\sPUBLIC\sKEY\sBLOCK-----/m.exec(iFile);
					var privateKey = /-----BEGIN\sPGP\sPRIVATE\sKEY\sBLOCK-----[\s\S]*-----END\sPGP\sPRIVATE\sKEY\sBLOCK-----/m.exec(iFile);
					try {
						var allPublicLines = publicKey[0].split(/\r\n|\n/);
						var allPrivateLines = privateKey[0].split(/\r\n|\n/);
					} catch (err) {
						swal('File Error', 'Files are not in the correct format.', 'error');
						reader.abort();
						return;
					}
					allPublicLines.map((line) => {
						//publines += line + '\n';
						publines = (publines == null) ? `${line}\n` : publines + `${line}\n`;
					});
					allPrivateLines.map((line) => {
						//privlines += line + '\n';
						privlines = (privlines == null) ? `${line}\n` : privlines + `${line}\n`;
					});
					_c.storage.local.set({
						publicKey: btoa(publines) //.replace('null', ''))
					});
					_c.storage.local.set({
						privateKey: btoa(privlines) //.replace('null', ''))
					});
					location.reload();
				}

				reader.onerror = (evt) => {
					swal('File error', evt.target.error.name, 'error');
					reader.abort();
				};
				reader.readAsText(file);
				//} catch(err) {
				//	swal('File Error', 'Files are not in the correct format.', 'error');
				//}
			},
			bmarkImportErrors: () => {
				_g.opgpbms.logDebug('Running bmarkImportErrors');
				swal('Bookmark Import Information', _g.sessionStorage.getItem('BmarkImportErrors'), 'info');
				sessionStorage.removeItem('BmarkImportErrors');
			},
			importKey: () => {
				///////////////////////
				/////  importKey  /////
				///////////////////////
				///// Upload      /////
				///// Encryption  /////
				///// Keys        /////
				///////////////////////
				_g.opgpbms.logDebug('Running importKey');
				_g.opgpbms.geid('importkeys').click();
			},
			importBookmarks: () => {
				/////////////////////////////
				/////  importBookmarks  /////
				/////////////////////////////
				///// Upload Encryption /////
				///// bookmrks          /////
				/////////////////////////////
				_g.opgpbms.logDebug('Running importBookmarks');
				_g.opgpbms.geid('importbmks').click();
			},
			generateKeyPair: () => {
				/////////////////////////////
				/////  generateKeyPair  /////
				/////////////////////////////
				///// Generate          /////
				///// Encryption Keys   /////
				/////////////////////////////
				_g.opgpbms.logDebug('Running generateKeyPair');
				var newEmail = _g.opgpbms.geid("new-email").value;
				var newPassword = _g.opgpbms.geid("new-password").value;
				var newPasswordRepeat = _g.opgpbms.geid("new-password-repeat").value;

				/* Verify private key encryption password. */
				if(newPassword !== "" && newEmail !== "") {
					if(newPassword === newPasswordRepeat) {
						_g.opgpbms.geid("setup-account").innerHTML = "Generating keys";
						_g.opgpbms.geid("setup-account").disabled = true;

						/* Prepare to generate keypair. */
						var options = {
							userIds: [{
								name: 'Openpgpjs Bookmark user',
								email: newEmail
							}],
							numBits: 4096,
							passphrase: newPassword
						};

						/* Generate keypair, store, and reload the page. */
						openpgp.generateKey(options).then((key) => {
							var privkey = key.privateKeyArmored;
							var pubkey = key.publicKeyArmored;
							_c.storage.local.set({
								privateKey: btoa(privkey)
							});
							_c.storage.local.set({
								publicKey: btoa(pubkey)
							});
							location.reload();
						});
					} else {
						swal('Password Error', 'Passwords do not match.', 'error');
						_g.opgpbms.gecn('swal-text')[0].style.textAlign = "center";
					}
				} else {
					swal('Password Error', 'Password cannot be null', 'error');
					_g.opgpbms.gecn('swal-text')[0].style.textAlign = "center";
				}
			},
			toggleNewEmailInput: () => {
				/////////////////////////////////
				/////  toggleNewEmailInput  /////
				/////////////////////////////////
				///// Input box background  /////
				///// text                  /////
				/////////////////////////////////
				_g.opgpbms.logDebug('Running toggleNewEmailInput');
				var targetvalue = _g.opgpbms.geid('new-email').value;
				_g.opgpbms.geid('new-email').addEventListener('focus', (e) => {
					if(e.target.value === targetvalue && e.target.value !== "") {
						e.target.value = "";
					}
				});
				_g.opgpbms.geid('new-email').addEventListener('blur', (e) => {
					if(e.target.value !== "") {
						e.target.classList.remove('empty-input');
					} else if(e.target.value === "") {
						e.target.value = targetvalue;
						e.target.classList.add('empty-input');
					}
				});
			},
			clearOBSession: () => {
				////////////////////////////
				/////  clearOBSession  /////
				////////////////////////////
				///// CLEAR SESSION    /////
				////////////////////////////
				_g.opgpbms.logDebug('Running clearOBSession');
				_g.sessionStorage.removeItem('openpgplist');
				_g.sessionStorage.removeItem('openpgpjs_loggedin');
				_g.sessionStorage.removeItem('tgp');
			},
			loginEnterKey: (e) => {
				///////////////////////////
				/////  loginEnterKey  /////
				///////////////////////////
				///// Enter Key Login /////
				///////////////////////////
				_g.opgpbms.logDebug('Running loginEnterKey');
				if((e.keyCode || e.which) == 13) {
					_g.opgpbms.geid('login-account').click();
				}
			},
			removedBookmarkWorker: (id, removeInfo) => {
				_g.opgpbms.logDebug('Running removedBookmarkWorker');
				_g.console.log("Item: " + id + " removed");
				_g.console.log("Title: " + removeInfo.node.title);
				_g.console.log("Url: " + removeInfo.node.url);
			},
			createdBookmarkWorker: (a, b) => {
				///////////////////////////////////
				/////  createdBookmarkWorker  /////
				///////////////////////////////////
				///// Adds bookmarks to the   /////
				///// current session         /////
				///// background page         /////
				///////////////////////////////////
				_g.opgpbms.logDebug('Running createBookmarkWorker');
				var continuation = () => {
					_c.storage.local.get('EBMFOLDER', (result) => {
						var EBMFOLDER = JSON.parse(atob(result.EBMFOLDER));
						var EBMID = EBMFOLDER.id;
						if(b.parentId != EBMID) return;
						var list = _g.opgpbms.gqs("#bookmarklist");
						var tbody = _g.opgpbms.gqs("#bookmarklist > tbody");
						//(async () => {
						//var dec = await _g.opgpbms.dBkmk(b.title);
						_g.opgpbms.dBkmk(b.title).then((dec) => {
							var dt = dec.split(/\$\|\$/)[0];
							var du = dec.split(/\$\|\$/)[1];
							_c.bookmarks.getChildren(EBMID, (bookmarks) => {
								var id = bookmarks[bookmarks.length - 1].id;
								var node = `<tr id="_${id}">` +
									`<td>` +
									`<a href="${du}" target="_blank">${dt}</a>` +
									`</td>` +
									`<td>` +
									`<a id="dlt" class="_${id}" href="#">Delete</a>` +
									`</td></tr>`;
								tbody.innerHTML += node;
								var list = _g.opgpbms.gqs('#bookmarklist').innerHTML;
								_g.sessionStorage.setItem('openpgplist', list);
								var db = _g.sessionStorage.getItem('db');
								db = JSON.parse(db);
								db = db.concat({
									'id': id,
									'title': dt,
									'url': du
								});
								_g.sessionStorage.setItem('db', JSON.stringify(db));
								try {
									var m = dtable.rows().data();
									m = [].slice.call(m);
									var tbody2 = _d.createElement('tbody');
									m.forEach((e) => {
										tbody2.innerHTML += (`<tr id="${e['DT_RowId']}"><td>${e[0]}</td><td>${e[1]}</td></tr>`)
									});
									_g.sessionStorage.setItem('dataTable', tbody2.innerHTML);
								} catch (err) {}
							});
						}).catch(function(error) {
							_g.opgpbms.logDebug(error);
						});
						//})();
					});
				}
				if(!_g.opgpbms.isEncryptedBMsSet) {
					_g.opgpbms.getEBMID().then(() => {
						continuation();
					}).catch(function(error) {
						_g.opgpbms.logDebug(error);
					});
				} else {
					continuation();
				}
				/*if(!_g.opgpbms.isEncryptedBMsSet) {
					_g.opgpbms.getEBMID();
					var cycle = _g.setInterval(() => {
						if(_g.opgpbms.isEncryptedBMsSet) {
							continuation();
							_g.clearInterval(cycle);
						}
					}, 100);
				} else {
					continuation();
				} */
			}
		}
	}();
	_g.opgpbms = opgpbms;
	_d.addEventListener('DOMContentLoaded', () => {
		///////////////////////////
		///// Document loaded /////
		///// functions       /////
		///////////////////////////
		openpgp.config.aead_protect = true;
		_g.opgpbms.toggleNewEmailInput();
		_g.opgpbms.checkForNewUser();
		_g.opgpbms.geid('login-account').addEventListener('click', _g.opgpbms.loginToAcct);
		_g.opgpbms.geid('login-password').addEventListener('keyup', _g.opgpbms.loginEnterKey);
		_g.opgpbms.geid('i-checkbox').addEventListener('change', () => {
			if(_g.opgpbms.geid('i-checkbox').checked) {
				_g.opgpbms.geid('p-importbms').classList.remove('o-hidden');
				_g.opgpbms.geid('b-importbms').addEventListener('click', _g.opgpbms.importBookmarks);
			} else {
				_g.opgpbms.geid('p-importbms').classList.add('o-hidden');
			}
		});
		_g.opgpbms.geid('ipass-checkbox').addEventListener('change', () => {
			if(_g.opgpbms.geid('ipass-checkbox').checked) {
				_g.opgpbms.geid('p-changepwd').classList.remove('o-hidden');
				_g.opgpbms.geid('p-changepwd').style.display = 'grid';
				_g.opgpbms.geid('b-changepwd').addEventListener('click', _g.opgpbms.cpd);
			} else {
				_g.opgpbms.geid('p-changepwd').classList.add('o-hidden');
				_g.opgpbms.geid('p-changepwd').style.display = 'none';
			}
		});
		_g.opgpbms.getn('summary')[0].addEventListener('click', () => {
			if(_g.opgpbms.getn('details')[0].getAttribute('open') === "") {
				_g.opgpbms.geid('i-checkbox').checked = false;
				_g.opgpbms.geid('ipass-checkbox').checked = false;
				_g.opgpbms.geid('p-changepwd').classList.add('o-hidden');
				_g.opgpbms.geid('p-changepwd').style.display = 'none';
				_g.opgpbms.geid('p-importbms').classList.add('o-hidden');
			}
		});
		_g.opgpbms.geid('d-bookmarklist').addEventListener('click', (e) => {
			//_d.body.addEventListener('click', (e) => {
			if(e.target.id == 'dlt') {
				var id = /_(.*)/.exec(e.target.classList[0])[1];
				_g.aid = id;
				_c.bookmarks.remove(id);
				//_g.console.log($('#_' + id));
				_g.dtable.row($('#_' + id)).remove();
				_g.opgpbms.geid('_' + id).remove();
				_g.dtable.draw();
				var list = _g.opgpbms.gqs('#bookmarklist').innerHTML;
				_g.sessionStorage.setItem('openpgplist', list);
				var db = JSON.parse(_g.sessionStorage.getItem('db'));
				db.splice(db.length - 1, db.length - 1);
				_g.sessionStorage.setItem('db', JSON.stringify(db));
				syncBookmarks();
			}
		});
		_g.opgpbms.geid('b-importkey').addEventListener('click', _g.opgpbms.importKey);
		_g.opgpbms.geid('importkeys').addEventListener('change', _g.opgpbms.handleImportKeys, false);
		_g.opgpbms.geid('importbmks').addEventListener('change', _g.opgpbms.handleImportBkmks, false);
		if(_g.sessionStorage.getItem('BmarkImportErrors') != null) {
			_g.opgpbms.bmarkImportErrors();
		}
	});
	_d.addEventListener('DOMNodeInserted', (evt) => {
		if(evt.target.tagName == 'TR' && evt.target.getAttribute('role') == null) {
			try {
				var ex = _d.getElementById('bookmarklist');
				if($.fn.DataTable.fnIsDataTable(ex)) {
					_g.target = evt.target;
					var bmarktitle = evt.target.getElementsByTagName('td')[0].getElementsByTagName('a')[0].innerText;
					bmarktitle = `Title:\n\n${bmarktitle}`;
					swal({
						title: 'Bookmark Added',
						text: bmarktitle,
						icon: 'success',
						timer: 5000
					});
					dtable.row.add(evt.target).draw();
				}
			} catch (err) {}
		}
	}, true);
	var get = {
		gtgp: () => {
			var r = _g.sessionStorage.getItem('tgp');
			var c = JSON.parse(r);
			var o = atob((c.endplan.substring(0, c.length)).split('').reverse().join(''));
			var tgp = o.split('').reverse().join('');
			return tgp;
		}
	}
	Object.setPrototypeOf(_g.opgpbms, get);
	_c.bookmarks.onCreated.addListener(_g.opgpbms.createdBookmarkWorker);
	//_c.bookmarks.onRemoved.addListener(_g.opgpbms.removedBookmarkWorker);
	//_c.bookmarks.onChanged.addListener(_g.opgpbms.removedBookmarkWorker);
})(window, document, chrome);