(() => {
	var popupProcesses = {
		showBookmarks: () => {
			var openbookmarks = document.getElementById('openbookmarks');
			openbookmarks.addEventListener('click', () => {
				chrome.tabs.create({
					url: "/html/bookmarks.html"
				});
			});
		},
		addExportKeys: () => {
			var exportkeys = document.getElementById('exportkeys');
			exportkeys.addEventListener('click', () => {
				chrome.storage.local.get('privateKey', (priv) => {
					var privkey = atob(priv.privateKey);
					var lines = null;
					var allLines = privkey.split(/\r\n|\n/);
					allLines.map((line) => {
						lines = (lines === null) ? line + '\r\n' : lines + line + '\r\n';
					});
					privkey = lines.trim();
					chrome.storage.local.get('publicKey', (pub) => {
						var pubkey = atob(pub.publicKey);
						var lines = null;
						var allLines = pubkey.split(/\r\n|\n/);
						allLines.map((line) => {
							lines = (lines === null) ? line + '\r\n' : lines + line + '\r\n';
						});
						pubkey = lines.trim();
						var combinedKeys = pubkey + '\r\n\r\n' + privkey;
						chrome.tabs.query({
							active: true,
							currentWindow: true
						}, (tabs) => {
							chrome.tabs.sendMessage(tabs[0].id, {
								sendkeys: combinedKeys
							}, (response) => {
								//window.console.log(response.gotkeys);
							});
						});
					});
				});
			});

		},
		isOtherBMsSet: false,
		isEncryptedBMsSet: false,
		getOtheBookmarks: () => {
			chrome.bookmarks.getTree((bmTree) => {
				bmTree[0].children.forEach((child) => {
					if(child.title === "Other bookmarks") {
						chrome.storage.local.set({
							OTHERBOOKMARKS: child
						});
						popupProcesses.isOtherBMsSet = true;
					}
				})
			});
		},
		getEBMID: () => {
			///////////////////////////////
			/////   chrome.getEBMID   /////
			///////////////////////////////
			///// Get the ID  of the  ///// 
			///// Encrypted Bookmarks ///// 
			///// folder and setItem  ///// 
			///////////////////////////////
			var continuation = () => {
				chrome.storage.local.get('OTHERBOOKMARKS', (result) => {
					chrome.bookmarks.getChildren(result.OTHERBOOKMARKS.id, (results) => {
						var EBMFOLDER = {};
						for(var i = 0; i < results.length; i++) {
							if(results[i].title === "Encrypted Bookmarks") {
								EBMFOLDER = btoa(JSON.stringify(results[i]));
								chrome.storage.local.set({
									EBMFOLDER: EBMFOLDER
								});
								popupProcesses.isEncryptedBMsSet = true;
							}
						}
					});
				});
			}
			if(!popupProcesses.isOtherBMsSet) {
				popupProcesses.getOtheBookmarks();
				var cycle = setInterval(() => {
					if(popupProcesses.isOtherBMsSet) {
						continuation();
						clearInterval(cycle);
					}
				}, 100);
			} else {
				continuation();
			}
		},
		addExportBookmarks: () => {
			var exportbmks = document.getElementById('exportbmks');
			exportbmks.addEventListener('click', () => {
				popupProcesses.getEBMID();
				var lines = null;
				var EBMFOLDER = {};
				chrome.storage.local.get('EBMFOLDER', (result) => {
					EBMFOLDER = JSON.parse(atob(result.EBMFOLDER));
					var i = 1;
					chrome.bookmarks.getChildren(EBMFOLDER.id, (results) => {
						results.forEach((item) => {
							lines = (lines === null) ? `[Bookmark${i}]` + '\r\n' + item.title + '\r\n' : lines + `[Bookmark${i}]` + '\r\n' + item.title + '\r\n';
							i++
						});
					});
					chrome.tabs.query({
						active: true,
						currentWindow: true
					}, (tabs) => {
						chrome.tabs.sendMessage(tabs[0].id, {
							sendBookmarks: lines
						}, (response) => {
							//window.console.log(response.gotBookmarks);
						});
					});
				});
			});
		},
		syncBookmarks: () => {
			var syncbmks = document.getElementById('syncbmks');
			syncbmks.addEventListener('click', () => {
				chrome.tabs.create({
					url: "/html/sync.html"
				});
			});
		},
		addOpenpgpBookMark: () => {
			document.getElementById('addbookmark').addEventListener('click', () => {
				chrome.tabs.query({
					"active": true,
					"lastFocusedWindow": true
				}, (tabs) => {
					if(/(chrome|opera|chrome-extension):/.test(tabs[0].url)) {
						var newalert = `Protocol: ${/.*:\/\//.exec(tabs[0].url)}\n\n` +
							`Bookmarking of the following protocols is not allowed:\n` +
							`     chrome://\n` +
							`     opera://\n` +
							`     chrome-extension://\n`
						chrome.tabs.sendMessage(tabs[0].id, {
							sendAlert: {
								'header': 'Bookmark Error',
								'text': `${newalert}`,
								'type': 'error'
							}
						}, (response) => {
							//window.console.log(response.gotAlert);
						});
						return;
					} else {
						var cssText = `html,body {
							width: 350px;
							height: 95px;
						}`;
						var style = document.createElement('style');
						style.id = 'openpgppopup'
						var cssTextNode = document.createTextNode(cssText);
						style.appendChild(cssTextNode);
						document.head.appendChild(style);
						document.getElementById('container').style.display = 'none';
						document.getElementById('bkmkwindow').style.display = 'block';
						document.getElementById('bkmk').value = tabs[0].title;
						document.getElementById('bkmkbtn').addEventListener('click', () => {
							var response = document.getElementById('bkmk').value;
							document.getElementById('openpgppopup').remove();
							document.getElementById('container').style.display = 'block';
							document.getElementById('bkmkwindow').style.display = 'none';
							chrome.storage.local.get('EBMFOLDER', (result) => {
								var EBMFOLDER = JSON.parse(atob(result.EBMFOLDER));
								var EBMID = EBMFOLDER.id;
								chrome.storage.local.get('publicKey', (keys) => {
									var title = response;
									var url = tabs[0].url;
									var joined = title + '$|$' + url;
									var options = {
										data: joined,
										publicKeys: openpgp.key.readArmored(atob(keys.publicKey)).keys
									};
									openpgp.encrypt(options).then((ciphertext) => {
										var base64joined = btoa(ciphertext.data);
										chrome.bookmarks.create({
											'title': base64joined,
											'url': 'http://pgp.encyrpyted.bookmark',
											'parentId': EBMID
										});
									});
								});
							});
							chrome.tabs.sendMessage(tabs[0].id, {
								sendAlert: {
									header: 'Bookmark Added',
									text: `Title:\n${response}`,
									type: 'success'
								}
							});
						});
					}
				});
			});
		},
		checkLogin: () => {
			var n;
			chrome.storage.local.get('LoginTime', (lt) => {
				window.ltime = lt;
				var loginTime = new Date(lt.LoginTime);
				var now = new Date();
				var diff = +now - +loginTime;
				var TotalMilliseconds = diff;
				var TotalSeconds = TotalMilliseconds / 1000;
				if(TotalSeconds / 60 >= 1) {
					var minutes = TotalSeconds / 60;
					var Minutes = parseInt(minutes);
					var TotalMinutes = minutes;
				} else {
					var Minutes = parseInt("");
					var TotalMinutes = parseInt("");
				}
				if(Minutes !== NaN) {
					var Seconds = parseInt(TotalSeconds % 60);
					var TotalTime = `${Minutes} min, ${Seconds} sec`;
				} else {
					var Seconds = parseInt(TotalSeconds);
					TotalTime = `${Seconds} seconds`;
				}

				console.log(now.getHours() - loginTime.getHours());
				console.log('Logged in for ' + parseInt((diff/1000)) + ' seconds.');
				if((diff/1000) < 3600) {
					chrome.storage.local.get('privateKey', (e) => {
						if(e.privateKey) {
							if(document.getElementById('addbookmark').disabled) {
								document.getElementById('addbookmark').disabled = false;
							}
							if(document.getElementById('exportkeys').disabled) {
								document.getElementById('exportkeys').disabled = false;
							}
							if(document.getElementById('exportbmks').disabled) {
								document.getElementById('exportbmks').disabled = false;
							}
							if(document.getElementById('syncbmks').disabled) {
								document.getElementById('syncbmks').disabled = false;
							}
							document.getElementById('logintime').innerHTML = 'Last login:<br>' + TotalTime;
						}
					})
				}
			});
		},
		buildPopup: () => {
			chrome.popupProcesses.checkLogin();
			chrome.popupProcesses.addOpenpgpBookMark();
			chrome.popupProcesses.showBookmarks();
			chrome.popupProcesses.addExportKeys();
			chrome.popupProcesses.addExportBookmarks();
			chrome.popupProcesses.syncBookmarks();
			window.chromi = chrome;
			chrome.tabs.query({
				"active": true,
				"lastFocusedWindow": true
			}, (tabs) => {
				window.chromi.tab = tabs[0];
			});
			document.removeEventListener('DOMContentLoaded', chrome.popupProcesses.buildPopup);
		}
	}
	chrome.popupProcesses = popupProcesses;
})();

document.addEventListener('DOMContentLoaded', chrome.popupProcesses.buildPopup);