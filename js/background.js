var promptuser = () => {
	chrome.storage.local.get('publicKey', (verify) => {
		if(verify.publicKey !== undefined) {
			addBookmark();
		} else {
			chrome.tabs.create({
				url: "/html/bookmarks.html"
			});
		}
	});
};

var syncBookmarks = () => {
	chrome.storage.local.get('syncStorage', (result) => {
		if(result.syncStorage === 'dropbox') {
			dropboxManager.getSync().then((result) => {
				if(result) {
					dropboxManager.clients.set(dropboxManager.clients.type.dropbox);
					var client = dropboxManager.clients.get();
					dropboxManager.setdbx(client).then((response) => {
						var EBMFOLDER;
						var lines;
						chrome.storage.local.get('EBMFOLDER', (result) => {
							console.log("Running uploadBookmarks");
							if(result.EBMFOLDER === undefined) {
								console.log("Bookmarks are not set.");
								return;
							} else {
								EBMFOLDER = JSON.parse(atob(result.EBMFOLDER));
								var i = 1;
								console.log("EBMFOLDER ID: " + EBMFOLDER.id);
								chrome.bookmarks.getChildren(EBMFOLDER.id, async (results) => {
									results.forEach((item) => {
										lines = (lines == null) ? `[Bookmark${i}]` + '\r\n' + item.title + '\r\n' : lines + `[Bookmark${i}]` + '\r\n' + item.title + '\r\n';
										i++
									});
									var bytes = dropboxManager.stringToBytes(lines);
									console.log(bytes);
									dropboxManager.setdbx(client).then((bool) => {
										if(bool) {
											var options = {
												path: '/Openpgpjs_Bookmarks.js',
												contents: lines,
												mode: 'overwrite'
											}
											dropboxManager.dbx.filesUpload(options).then((response) => {
													console.log(JSON.stringify(response));
												})
												.catch((error) => {
													_g.opgpbms.logDebug(error);
													_g.opgpbms.logDebug(error);
													_g.opgpbms.logDebug(error);
												});
										}
									});
								});
							}
						});
					});
				}
			});
		}
	});
}

var addBookmark = () => {
	chrome.tabs.query({
		"active": true,
		"lastFocusedWindow": true
	}, (tabs) => {
		if(/(chrome|opera|chrome-extension):/.test(tabs[0].url)) {
			var newalert = `Protocol: ${/.*:\/\//.exec(tabs[0].url)}\n\n` +
				`Bookmarking of the following protocols is not allowed:\n` +
				`     chrome://\n` +
				`     opera://\n` +
				`     chrome-extension://\n`;
			chrome.tabs.sendMessage(tabs[0].id, {
				sendAlert: {
					'header': 'Bookmark Error',
					'text': newalert,
					'type': 'error'
				}
			}, (response) => {
				//window.console.log(response.gotAlert);
			});
			return;
		}
		var title = false;
		chrome.storage.local.get('EBMFOLDER', (result) => {
			var EBMFOLDER = JSON.parse(atob(result.EBMFOLDER));
			var EBMID = EBMFOLDER.id;
			chrome.storage.local.get('publicKey', (keys) => {
				var response = prompt("Set Bookmark Title", tabs[0].title);
				if(response) {
					title = response;
					var url = tabs[0].url;
					var joined = title + '$|$' + url;
					var options = {
						data: joined,
						publicKeys: openpgp.key.readArmored(atob(keys.publicKey)).keys
					}
					openpgp.encrypt(options).then((ciphertext) => {
						var base64joined = btoa(ciphertext.data);
						chrome.bookmarks.create({
							'title': base64joined,
							'url': 'http://pgp.encyrpyted.bookmark',
							'parentId': EBMID
						});
						chrome.tabs.sendMessage(tabs[0].id, {
							sendAlert: {
								'header': 'Bookmark Added',
								'text': `Title:\n\n${title}`,
								'type': 'success'
							}
						}, (response) => {
							//window.console.log(response.gotAlert);
						});
					});
				}
				syncBookmarks();
			});
		});
	});
}

var createBookmarkFolder = () => {
	chrome.bookmarks.getTree((bmTree) => {
		var other;
		var encryptedbookmarks = false;
		var foldername = "Other bookmarks";
		bmTree[0].children.forEach((child) => {
			if(/OPR/.test(window.navigator.userAgent)) {
				foldername = "Imported bookmarks";
			}
			if(child.title === foldername) {
				other = child;
				child.children.forEach((bookmark) => {
					if(bookmark.title === 'Encrypted Bookmarks') {
						encryptedbookmarks = true;
					}
				});
			}
		});
		if(!encryptedbookmarks) {
			chrome.bookmarks.create({
				'title': 'Encrypted Bookmarks',
				'parentId': other.id
			});
		}
	});
}

var CONTEXTMENUID = null;
var createContextMenu = () => {
	var context = "page";
	var optionsNew = {
		"title": "Bookmark with Openpgp Encryption",
		"contexts": [context],
		"id": "context" + context
	}
	var optionsUpdate = {
		"title": "Bookmark with Openpgp Encryption",
		"contexts": [context]
	}
	if(CONTEXTMENUID != null) {
		chrome.contextMenus.update(CONTEXTMENUID, optionsUpdate);
		console.log('CONTEXTMENUID update: ' + CONTEXTMENUID);
	} else {
		CONTEXTMENUID = chrome.contextMenus.create(optionsNew);
		console.log('CONTEXTMENUID created: ' + CONTEXTMENUID);
	}
	if(chrome.runtime.lastError) {
		console.error('ERROR: ' + chrome.runtime.lastError.message);
	}
}

chrome.tabs.onCreated.addListener((e) => {
	chrome.storage.local.get('bookmarks', (item) => {
		if(item.bookmarks) {
			if(chrome.extension.inIncognitoContext) {
				createContextMenu();
			}
			if(!chrome.extension.inIncognitoContext) {
				createContextMenu();
			}
		}
	});
});

/* Preform various tasks on first installation. */
chrome.runtime.onInstalled.addListener((details) => {
	if(details.reason == "install") {
		chrome.storage.local.set({
			bookmarks: {
				"bookmarks": []
			}
		});

		var context = "page";
		CONTEXTMENUID = chrome.contextMenus.create({
			"title": "Bookmark with Openpgp Encryption",
			"contexts": [context],
			"id": "context" + context
		});

		createBookmarkFolder();

		chrome.tabs.create({
			url: "/html/bookmarks.html"
		});
	}
});

var testrun = () => {
	chrome.tabs.query({
		"active": true,
		"lastFocusedWindow": true
	}, (tabs) => {
		chrome.tabs.executeScript(tabs[0].id, {
			code: `console.log("hello");`
		}, (result) => {
			console.log('bookmark deleted.')
		});
	});
}
chrome.contextMenus.onClicked.addListener(promptuser);

/////////////////////////// Other stuff //////////////////////////
var removedBookmarkWorker = (id, removeInfo) => {
	chrome.tabs.query({
		"active": true,
		"lastFocusedWindow": true
	}, (tabs) => {
		chrome.tabs.executeScript(tabs[0].id, {
			code: `console.log("Item: ${id} removed");console.log("Title: ${removeInfo.node.title}");console.log("Url: ${removeInfo.node.url}")`
		}, (result) => {
			console.log('bookmark deleted.')
		});
	});
}
chrome.bookmarks.onRemoved.addListener(removedBookmarkWorker);